from sqlalchemy import Column
from sqlalchemy import Integer, String, Text, DateTime
from sqlalchemy.sql import func

from core.db import Base


class Task(Base):

    __tablename__ = "tasks"

    id = Column(Integer, primary_key=True)
    title = Column(String(255), nullable=False)
    body = Column(Text)
    date_created = Column(DateTime(timezone=True), server_default=func.now())
    date_updated = Column(DateTime(timezone=True), onupdate=func.now())
