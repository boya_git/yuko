import os

from dotenv import load_dotenv


# .env must be at one lvl with endpoint.
load_dotenv()


class Config:
    """ Default app configuration. """

    DB_NAME = os.getenv("DB_NAME")
