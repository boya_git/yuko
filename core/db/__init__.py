from sqlalchemy import create_engine
from sqlalchemy.orm import declarative_base

from core.config import Config


engine = create_engine(f"sqlite:///{Config.DB_NAME}.db")
Base = declarative_base()
