from string import Template


greetings_template = Template("""
    Hello, my name is Yuko. Today I will help you in any way I can :)
""")
introduce_question = Template("""
    Could you introduce yourself, please?
""")
acquaintance = Template("""
    Nice to meet you, $user_name :)
""")
notify_question = Template("""
    What should I remind you of?
""")
time_question = Template("""
    When should I remind you?
""")
notify_accepted = Template("""
    Fine, I'll remind you of $notify through $time! Something else?
""")
goodbuy_message = Template("""
    Looks like we're done, goodbye :)
""")

if __name__ == '__main__':
    print(greetings_template.substitute())
    print(introduce_question.substitute())
    user_name = input()
    print(acquaintance.substitute(user_name=user_name))
    while (notify := input(notify_question.substitute())) != "That's all":
        time = input(time_question.substitute())
        print(notify_accepted.substitute(notify=notify, time=time))
    print(goodbuy_message.substitute())
